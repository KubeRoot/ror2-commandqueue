### 1.6.1 - Bugfix

    - Fix issue where items would not be automatically selected from command droplets.
      This was caused by trying to get a reference to the command picker panel too early, which wasn't an issue when hot-reloading in development.

### 1.6.0 - Game update

    - Update for V1.3.1 - Seekers of the Storm update

### 1.5.0 - Game update

    - Update for Devotion Update

### 1.4.0 - Game update

    - Update for SotV patch 1.2.3

### 1.3.3 - Hotfix

    - Fix item icons having the wrong scale when advancing to the next level, which caused items in the queue to start overlapping.

### 1.3.2 - Looping mode

    - Add looping mode. If the button is enabled, when commandqueue chooses an item, the chosen item is added back at the end of the queue.

### 1.3.1 - Plug my Ko-fi

### 1.3.0 - Game update, extra features

    - Update the mod for the Survivors of the Void update
    - Add drag'n'drop for items in the queue
    - Add an option to remove a whole stack of items in the queue with right-click, enabled by default
    - Merge adjacent stacks of the same item in the queue, happens when an item between them is deleted
    - Stop queue items getting focus when clicked. Will prevent issues where game doesn't respond to input until you click.

### 1.1.0 - Extra features

    - Add expanded menu option (enabled by default)
    - Add support for Lunar and Boss item queues (disabled by default)
    - Change source of item rarities, should help compatibility in some cases
    - Minor improvements to UI (figured out pivots, things can be more dynamic)

### 1.0.0 - Initial release


### 1.0.1 - Remove a debugging message from being printed in chat

### 1.0.0 - Initial release