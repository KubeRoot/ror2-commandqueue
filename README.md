# CommandQueue

Creates a menu on the scoreboard that lets you queue up items to have picked up automatically when you open a command cube

![Game screenshot](https://i.imgur.com/ma2kgok.png)

[Support me on Ko-fi](https://ko-fi.com/kuberoot)

## Features:

-   A full intuitive GUI accessible through a newly added tab on the scoreboard
-   Add items to the end of the queue and remove items from anywhere in the queue
-   Queues for the three item rarities, lunar and boss items, as well as void variants, available in separate tabs
    -   Lunar, boss and void item queues are disabled by default, can be enabled in config
-   Automatically selects items when you interact with a command cube, as long as it's available
    -   If none of the items at the end of the queues are available, the menu opens as normal
-   You can now make the queue loop, allowing you to farm the same sequence of items repeatedly

## Limitations

There is no controller support - the mod hasn't been tested with a controller and the UI is not in any way made to work with a controller.

The mod has not been comprehensively tested with the latest update, there might be issues or errors.

## Installation

Copy the `CommandQueue` folder to `Risk of Rain 2/BepInEx/plugins`

## Attribution

Looping icon based on [repeat from FontAwesome](https://fontawesome.com/icons/repeat?s=solid).
