﻿using System;
using System.Collections;
using System.Security.Permissions;
using BepInEx;
using RoR2;
using RoR2.UI;
using UnityEngine;

/*
    string directoryName = Path.GetDirectoryName(ConfigFilePath);
    if (directoryName != null) Directory.CreateDirectory(directoryName);

    using (var writer = new StreamWriter(ConfigFilePath, false, Encoding.UTF8))

TODO:
Current tasks:
- Provide a way to setup (per-character) initial queues
 - Adapt ItemSetConfigWrapper from AutoItemPickup
 - Generate a short description in comments
 - Generate sections named after characters
 - Parse section contents for items
  - Decide on format
  ? \s* ItemName \s* \* Count \s* ,

Future tasks:
- Maybe create UI that shows other players' queue
 - Should players broadcast their queues even if server isn't aware?
- Toggle button that makes popped items get put back at the end of the queue, for repeatedly picking up the same items without setting them over again

*/

#pragma warning disable CS0618 // Type or member is obsolete
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
#pragma warning restore CS0618 // Type or member is obsolete

namespace CommandQueue
{
    [BepInPlugin("com.kuberoot.commandqueue", "CommandQueue", "1.6.1")]
    public class CommandQueue : BaseUnityPlugin
    {
        public static event Action PluginUnloaded;
        public static bool IsLoaded;

        private static GameObject commandUIPrefab;

        public void Awake()
        {
            ModConfig.InitConfig(Config);
            ModConfig.enabledTabs.SettingChanged += (_, __) => FakeReload();
            ModConfig.bigItemButtonContainer.SettingChanged += (_, __) => FakeReload();
            ModConfig.bigItemButtonScale.SettingChanged += (_, __) => FakeReload();
        }

        private bool isFakeReloading = false;

        private void FakeReload()
        {
            if (isFakeReloading) return;
            isFakeReloading = true;
            IEnumerator doFakeReload()
            {
                yield return 0;
                OnDisable();
                yield return 0;
                OnEnable();
                isFakeReloading = false;
            }
            StartCoroutine(doFakeReload());
        }

        public void OnEnable()
        {
            commandUIPrefab = RoR2.Artifacts.CommandArtifactManager.commandCubePrefab?.GetComponent<PickupPickerController>().panelPrefab;
            IsLoaded = true;

            On.RoR2.PickupPickerController.OnDisplayBegin += HandleCommandDisplayBegin;
            On.RoR2.UI.ScoreboardController.Awake += ScoreboardController_Awake;
            On.RoR2.Artifacts.CommandArtifactManager.Init += CommandArtifactManager_Init;
            QueueManager.Enable();
            
            foreach (var component in FindObjectsOfType<HUD>()) component.scoreboardPanel.AddComponent<UIManager>();
        }

        public void OnDisable()
        {
            IsLoaded = false;
            PluginUnloaded?.Invoke();
            On.RoR2.PickupPickerController.OnDisplayBegin -= HandleCommandDisplayBegin;
            On.RoR2.UI.ScoreboardController.Awake -= ScoreboardController_Awake;
            On.RoR2.Artifacts.CommandArtifactManager.Init -= CommandArtifactManager_Init;
            QueueManager.Disable();
        }

        private void CommandArtifactManager_Init(On.RoR2.Artifacts.CommandArtifactManager.orig_Init orig)
        {
            orig();

            IEnumerator coroutine()
            {
                while (RoR2.Artifacts.CommandArtifactManager.commandCubePrefab == null)
                    yield return new WaitForSeconds(1);
                
                commandUIPrefab = RoR2.Artifacts.CommandArtifactManager.commandCubePrefab?.GetComponent<PickupPickerController>().panelPrefab;
            }

            StartCoroutine(coroutine());
        }

        private void HandleCommandDisplayBegin(On.RoR2.PickupPickerController.orig_OnDisplayBegin orig, PickupPickerController self, NetworkUIPromptController networkUIPromptController, LocalUser localUser, CameraRigController cameraRigController)
        {
            if (self.panelPrefab == commandUIPrefab)
            {
                foreach(var (tier, index) in QueueManager.PeekAll())
                {
                    if (self.IsChoiceAvailable(index))
                    {
                        QueueManager.Pop(tier);
                        PickupPickerController.Option[] options = self.options;

                        for (int j = 0; j < options.Length; j++)
                        {
                            if(options[j].pickupIndex == index && options[j].available)
                            {
                                IEnumerator submitChoiceNextFrame()
                                {
                                    yield return 0;
                                    self.SubmitChoice(j);
                                }
                                self.StartCoroutine(submitChoiceNextFrame());
                                break;
                            }
                        }
                        
                        return;
                    }
                }
            }

            orig(self, networkUIPromptController, localUser, cameraRigController);
        }

        private void ScoreboardController_Awake(On.RoR2.UI.ScoreboardController.orig_Awake orig, ScoreboardController self)
        {
            self.gameObject.AddComponent<UIManager>();
            orig(self);
        }
    }
}